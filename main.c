/*
  GNU GENERAL PUBLIC LICENSE
  Nome: Aprendendo a Reciclar Beta 1.0
  Author: Daniel Antonio Torres Cesario (danielatcesario@gmail.com)
  Date: Finalizado em 29/08/11 19:37 
  Description: Jogo com o objetivo de ensinar crian�as sobre as cores de
  reciclagem de materiais. Trabalho desenvolvido para a mat�ria de Linguagem de
  Programa��o 1, do segundo semestre de 2011, da Faculdade de Tecnologia FATEC
  de S�o Jos� dos Campos.
*/

#include <allegro.h>
#include <time.h>

void telaInicial(){ //Fun��o da Tela Inicial
    PALETTE ini;
    BITMAP *inicial = load_bitmap("fundos/inicial.bmp", ini);
    draw_sprite(screen, inicial, 0, 0);
    destroy_bitmap(inicial);     
}

void telaCreditos(){ //Fun��o da Tela de Creditos
    PALETTE cred;
    BITMAP *creditos = load_bitmap("fundos/creditos.bmp", cred);
    draw_sprite(screen, creditos, 0, 0);
    destroy_bitmap(creditos);     
}

void telaAjuda(){ //Fun��o da Tela de Ajuda
    PALETTE ajd;
    BITMAP *ajuda = load_bitmap("fundos/ajuda.bmp", ajd);
    draw_sprite(screen, ajuda, 0, 0);     
    destroy_bitmap(ajuda);
}

void telaJogo(){ //Fun��o da Tela de Jogo
    PALETTE jog, l1;
      
    //Fundo da tela
    BITMAP *jogo = load_bitmap("fundos/jogo.bmp", jog);
    draw_sprite(screen, jogo, 0, 0);
    destroy_bitmap(jogo);  
                     
}

void telaResultado(int v[]){     
             
     int i; //Inicia o contador
     int contaPontos = 0; //Inicia a vari�vel para contagem de pontos
     
     //Loop para ler quantos pontos certos (1) contem dentro do vetor recebido
     for(i=0;i<=10;i++){
           if(v[i] == 1){
                 contaPontos += 1; //Soma 1 ponto na vari�vel de contagem
           }              
     }
     
     //Array com os arquivos das notas
     char notas[11][100] = {
                            "notas/00.bmp",
                            "notas/01.bmp",
                            "notas/02.bmp",
                            "notas/03.bmp",
                            "notas/04.bmp",
                            "notas/05.bmp",
                            "notas/06.bmp",
                            "notas/07.bmp",
                            "notas/08.bmp",
                            "notas/09.bmp",
                            "notas/10.bmp"
                          };     
     
     //Fundo da tela
     BITMAP *resultado = load_bitmap("fundos/resultado.bmp", NULL);
     draw_sprite(screen, resultado, 0, 0);
     destroy_bitmap(resultado);

     //Executa at� pressionar a tecla V
     while(!key[KEY_V]){
              
         //Mostrando pontos usando a array de notas e a vari�vel de contagem
         BITMAP *mostraPontos = load_bitmap(notas[contaPontos], NULL);
         draw_sprite(screen, mostraPontos, 325, 390);
         destroy_bitmap(mostraPontos);
     }
     
}

void telaInfo(int r){
     
         //Array com os nomes dos arquivos das figuras de informa��es
         char infos[7][100] = {
                                "infos/info_copoplastico.bmp",
                                "infos/info_lata.bmp",
                                "infos/info_longavida.bmp",
                                "infos/info_garrafavidro.bmp",
                                "infos/info_bolinhadepapel.bmp",
                                "infos/info_caixapapelao.bmp",
                                "infos/info_garrafapet.bmp"
                              }; 
                              
          
          //Mostrando a tela de Info
          BITMAP *info = load_bitmap(infos[r], NULL);
          draw_sprite(screen, info, 0, 172);
          destroy_bitmap(info);          
     
}

void somMenus(){
          SAMPLE *som = load_wav("sons/menu.wav");
    	  play_sample(som,255,90,1000,0);     
}

void somCerto(){
          SAMPLE *som1 = load_wav("sons/certo.wav");
    	  play_sample(som1,255,90,1000,0);     
}

void somErrado(){
          SAMPLE *som2 = load_wav("sons/errado.wav");
    	  play_sample(som2,255,90,1000,0);     
}

void somFundo(){
          MIDI *mid = load_midi("sons/fundo.mid");
          play_midi(mid,1);
}


volatile int FecharJogo = FALSE; 
void BotaoFechar(void) 
{ 
   FecharJogo = TRUE; 
} 
END_OF_FUNCTION(BotaoFechar) 

int main() {
    
    allegro_init(); 
    install_keyboard();
    set_color_depth(32);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED,800,600,0,0);     
    set_window_title("Aprendendo a Reciclar - AAP Linguagem de Programacao 1");
    install_sound(DIGI_AUTODETECT,MIDI_AUTODETECT,NULL);
    
    LOCK_FUNCTION(BotaoFechar); 
    set_close_button_callback(BotaoFechar); 
    
    somFundo();
    
    while (!FecharJogo) { //Abilitando o bot�o fechar com a fun��o		
		
        telaInicial(); //Chama fun��o da tela inicial
		
		
		if(key[KEY_ESC]){ //Habilitando o fechamente tamb�m pela tecla ESC
             BotaoFechar();
        }
		
		//Tela de Creditos
        if(key[KEY_C]){ //Se apertar o C chama a fun��o da tela de creditos
    	     somMenus();             
             while(!key[KEY_V]) { //Loop at� apertar o V para voltar a tela inicial
                  telaCreditos();
             }
        }
        
		//Tela de Ajuda
        if(key[KEY_A]){ //Se apertar o A chama a fun��o da tela de ajuda
    	     somMenus();
             while(!key[KEY_V]) { //Loop at� apertar o V para voltar a tela inicial
                  telaAjuda();
             }
        }
        
		//Tela de Jogo
        if(key[KEY_I]){ //Se apertar o A chama a fun��o da tela de jogo
    	     
             somMenus();
             int pontos[10]; //Array para armazenar os pontos 1=Certo / 0=Errado
             int cont=0; //Contador de tentativas
             
             //Array com os nomes dos arquivos das figuras
             char lixos[7][100] = {
                                    "lixos/lixo_copo.bmp",
                                    "lixos/lixo_lata.bmp",
                                    "lixos/lixo_caixasuco.bmp",
                                    "lixos/lixo_garrafavidro.bmp",
                                    "lixos/lixo_bolinhapapel.bmp",
                                    "lixos/lixo_caixapapelao.bmp",
                                    "lixos/lixo_garrafapet.bmp" 
                                  };
              
             //Tipos 1 = Pl�stico (Vermelho) / 2 = Vidro (Verde) / 3 = Papel (Azul) / 4 = Metal (Amarelo)
             int tipos[7] = {
                             1,
                             4,
                             3,
                             2,
                             3,
                             3,
                             1
                            };                                     
             
             while(!key[KEY_V]) { //Loop at� apertar o V para voltar a tela inicial
                  
                  telaJogo();//Chama a fun��o para criar a tela                  
                  
                  int x=265, y=190; //Posi��o inicial do objeto
                                    
                  int r = rand()%8; //Fazendo um rand para escolher uma figura aleat�ria
                  
                  //Mostrando a primeira figura
                  BITMAP *lixo = load_bitmap(lixos[r], NULL);
                  draw_sprite(screen, lixo, x, y);
                  destroy_bitmap(lixo);
                  
                  telaInfo(r);                 
                  
                  int loop=1; //Vari�vel do loop da jogada atual, ap�s jogar ela � cetada para 0 e inicia uma nova rodada
                              
                  while(loop == 1){ //While para n�o ficar piscando a tela
                      
                      BITMAP *lixo = load_bitmap(lixos[r], NULL); //Cria e carrega a imagem com base na array e no �ndice do rand
                                     
                      
                      
                      //Movimenta��o do objeto
                      if(key[KEY_RIGHT]){
                          telaJogo(); //Chama a tela para n�o ficar preto no fundo
                          telaInfo(r); 
                          x += 8; //Move para a direita
                          draw_sprite(screen, lixo, x, y); //desenha com as medidas certas
                          //destroy_bitmap(lixo);
                      }
                      
                      if(key[KEY_LEFT]){
                          telaJogo(); //Chama a tela para n�o ficar preto no fundo
                          telaInfo(r);
                          x -= 8; //Move para a esquerda
                          draw_sprite(screen, lixo, x, y); //desenha com as medidas certas
                          //destroy_bitmap(lixo);
                      }

                      if(key[KEY_UP]){
                          telaJogo(); //Chama a tela para n�o ficar preto no fundo
                          telaInfo(r);
                          y -= 8; //Move para a cima
                          draw_sprite(screen, lixo, x, y); //desenha com as medidas certas
                          //destroy_bitmap(lixo);
                      }                      
                      
                      if(key[KEY_DOWN]){
                          telaJogo(); //Chama a tela para n�o ficar preto no fundo
                          telaInfo(r);
                          y += 8; //Move para a baixo
                          draw_sprite(screen, lixo, x, y); //desenha com as medidas certas
                          //destroy_bitmap(lixo);
                      }
                      
                      
                       //Posi��o antiga: 15/142
                       //-30                     
                      //Colis�o lata verde (P1, P2, P3 e P4) Tipo 2
                      if((x >= 3) && (x <= 86)) { //P1
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 2) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();  
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      } 
                                            
                      if((x+120 >= 3) && (x+120 <= 86)) { //P2
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 2) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x >= 3) && (x <= 86)) { //P3
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 2) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x+120 >= 3) && (x+120 <= 86)) { //P4
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 2) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      
                      
                      
                      //Posi��o Antiga: 143/272
                      //-30
                      //Colis�o lata amarela (P1, P2, P3 e P4) Tipo 4
                      if((x >= 225) && (x <= 303)) { //P1
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 4) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      } 
                      
                      if((x+120 >= 225) && (x+120 <= 303)) { //P2
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 4) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x >= 225) && (x <= 303)) { //P3
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 4) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x+120 >= 225) && (x+120 <= 303)) { //P4
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 4) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      
                      
                      
                      //Posi��o Antiga: 273/406
                      //-30
                      //Colis�o lata vermelha (P1, P2, P3 e P4) Tipo 1
                      if((x >= 440) && (x <= 520)) { //P1
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 1) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      } 
                      
                      if((x+120 >= 440) && (x+120 <= 5250)) { //P2
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 1) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x >= 440) && (x <= 520)) { //P3
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 1) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x+120 >= 440) && (x+120 <= 520)) { //P4
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 1) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      } 
                      
                      
                      
                      
                      //Posi��o Antiga: 407/536
                      //-45
                      //Colis�o lata azul (P1, P2, P3 e P4) Tipo 3
                      if((x >= 641) && (x <= 740)) { //P1
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 3) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      } 
                      
                      if((x+120 >= 641) && (x+120 <= 740)) { //P2
                           if((y >= 501) && (y <= 590)){
                                 if(tipos[r] == 3) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x >= 641) && (x <= 740)) { //P3
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 3) { //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }
                      
                      if((x+120 >= 641) && (x+120 <= 740)) { //P4
                           if((y+120 >= 501) && (y+120 <= 590)){
                                 if(tipos[r] == 3 ){ //Compara com o tipo da caixa
                                      somCerto();
                                      pontos[cont] = 1;
                                      loop = 0;
                                 } else {
                                      somErrado();
                                      pontos[cont] = 0; 
                                      loop = 0;
                                 }
                                 destroy_bitmap(lixo);
                           }
                      }                                                                    
                      
                      
                      
                      if(key[KEY_V]) {// Cancela o jogo e volta ao menu inicial
                           break;
                      }
                                                                                     
                                                                
                  
                  }
                  
                  //break;
                  cont++;
                  
                  if(cont >= 10){ //Termina quando atinge o n�mero de rodadas                       
                       telaResultado(pontos);
                  }
                  
             }
        
        }                
        
	}	
	allegro_exit();
    return 0;
}
END_OF_MAIN()
